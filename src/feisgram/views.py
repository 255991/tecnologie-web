from django.shortcuts import render, redirect
from .forms import CustomSignupForm
from django.contrib.auth import authenticate, login
from profiles.models import Profile

def home(request):
    '''
        Utente loggato 
    '''
    user = request.user
    context = {
        'user' : user,
    }
    return render(request, 'main/home.html', context)

def signup(request):
    form = CustomSignupForm()
    if request.method == "POST":
        form = CustomSignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            first_name = form.cleaned_data.get("first_name")
            last_name = form.cleaned_data.get("last_name")
            email = form.cleaned_data.get("email")
            password = form.cleaned_data.get("password1")
            form.save()
            new_user = authenticate(username=username, password=password)
            if new_user is not None:
                login(request, new_user)
                currentUsr = Profile.objects.get(user=request.user)
                currentUsr.first_name = first_name
                currentUsr.last_name = last_name
                currentUsr.email = email
                currentUsr.save()
                return redirect('profiles:my-profile-view')
    context = {
        'form': form,
    }
    return render(request, 'account/signup.html', context)
