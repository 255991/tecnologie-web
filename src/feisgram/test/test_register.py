from django.test import TestCase
from django.urls import reverse

class BaseTest(TestCase):
    def setUp(self):
        self.register_url = reverse('account_signup')
        self.user = {
            'username': 'PaoloRossi',
            'first_name': 'Paolo',
            'last_name': 'Rossi',
            'email': 'paolorossi@gmail.com',
            'password1': 'PasswordSicura00!',
            'password2': 'PasswordSicura00!',
        }
        return super().setUp()

class RegisterTest(BaseTest):
    def test_can_view_page_correct(self):
        response = self.client.get(self.register_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'account/signup.html')

    def test_can_register_user(self):
        response = self.client.post(self.register_url,self.user,format='text/html')
        self.assertEqual(response.status_code, 302)

    def tearDown(self) -> None:
        del self.user
        return super().tearDown()