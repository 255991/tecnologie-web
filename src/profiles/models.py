import profile
from django.db import models
from django.shortcuts import reverse
from django.contrib.auth.models import User
from .utils import get_random_code
from django.template.defaultfilters import slugify
from django.db.models import Q

# Create your models here.

'''
    Estensione della tabella Profilo
'''

class ProfileManager(models.Manager):
    '''
        Funzione che restituisce tutti i profili al quale è possibile inviare una richiesta di amicizia 
        scartando coloro che fanno già parte della lista degli amici dell'utente collegato
    '''
    def get_all_profiles_free_to_invite(self, sender):
        '''
            Prelevo tutti i profili dal database escludendo quello dell'utente collegato
        '''
        profiles = Profile.objects.all().exclude(user=sender)
        '''
            Prelevo il profilo dell'utente collegato
        '''
        profile = Profile.objects.get(user=sender)
        '''
            Creo una query di interrogazione che mi filtra tutti i risultati in base al campo sender(mittente) e receiver(destinatario)
            cosi da ottenere tutte le righe della tabella Relationship dove sender = receiver = profilo collegato
        '''
        qs = Relationship.objects.filter(Q(sender=profile) | Q(receiver=profile))

        '''
            Inserisco in un insieme vuoto tutte le relazioni che mi permettono di confermare l'amicizia tra l'utente collegato
            e relativo amico
            La scelta di un insieme è dovuta al fatto di eliminare i duplicati nei risultati finali
        '''
        accepted = set([])
        for r in qs:
            if r.status == 'accepted':
                accepted.add(r.receiver)
                accepted.add(r.sender)
        
        '''
            Genero una lista prendendo in considerazione tutti i profili disponibili togliendo quelli presenti nella lista 
            amici
        '''
        notAccepted = [profile for profile in profiles if profile not in accepted]

        return notAccepted
        
    '''
        Funzione che restituisce tutti i profili tranne che quello collegato
    '''
    def get_all_profiles_exclude_me(self, me):
        profiles = Profile.objects.all().exclude(user=me)

'''
    Modello Profilo = Tabella Profilo all'interno del database
'''

class Profile(models.Model):
    first_name = models.CharField(max_length=200,blank=True) 
    last_name = models.CharField(max_length=200,blank=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(default="Insert your bio", max_length=400)
    email = models.EmailField(max_length=200,blank=True)
    country = models.CharField(max_length=200,blank=True)
    avatar = models.ImageField(default='avatar.png', upload_to='avatars/')

    friends = models.ManyToManyField(User, blank=True, related_name='friends')
    slug = models.SlugField(unique=True, blank=True)
    updated = models.DateTimeField(auto_now_add=True)
    created = models.DateTimeField(auto_now_add=True)

    '''
        Riga per estendere la classe profilo
    '''
    objects = ProfileManager()

    def __str__(self):
        return f"{ self.user.username } - { self.created.strftime('%d-%m-%y') }"

    def get_absolute_url(self):
        return reverse("profiles:profile-detail-view", kwargs={"slug": self.slug})
    
    '''
        Funzione che restituisce l'elenco di tutti gli amici di un determinato utente
    '''
    def get_all_friends(self):
        return self.friends.all()
    
    '''
        Funzione che restituisce il numero complessivo di amici di un determinato utente
    '''
    def get_all_friends_count(self):
        return self.friends.all().count()

    '''
        Funzione che ritorna il contenuto di tutti i post di un determinato utente
    '''
    def get_all_posts(self):
        return self.posts.all()

    '''
        Funzione che ritorna il numero complessivo dei post pubblicati da un determinato utente
    '''
    def num_posts_no(self):
        return self.posts.all().count()

    '''
        Funzione che ritorna il numero totale dei like
    '''
    def get_likes_given_no(self):
        likes = self.like_set.all()
        total_likes = 0
        for item in likes:
            if item.value == 'like':
                total_likes += 1
        return total_likes

    '''
        Funzione che ritorna il numero di like di un determinato post associati ad un determinato utente
    '''
    def get_likes_received_no(self):
        posts = self.posts.all()
        total_likes = 0
        for item in posts:
            total_likes += item.liked.all().count()
        return total_likes

    __initial_first_name = None
    __initial_last_name = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__initial_first_name = self.first_name
        self.__initial_last_name = self.last_name

    '''
        Funzione che mi permette di controllare se un utente con stesso nome e cognome è gia presente, in tal
        caso per garantire l'univocità di un utente viene aggiunto un id casuale generato da una funzione presente nel file 
        utils.py
    '''
    def save(self, *args, **kwargs):
        ex = False
        to_slug = self.slug
        if self.first_name != self.__initial_first_name or self.last_name != self.__initial_last_name or self.slug == "":
            if self.first_name and self.last_name:
                to_slug = slugify(str(self.first_name) + " " + str(self.last_name))
                ex = Profile.objects.filter(slug = to_slug).exists()
                while ex:
                    to_slug = slugify(to_slug + " " + str(get_random_code()))
                    ex = Profile.objects.filter(slug = to_slug).exists()
            else:
                to_slug = str(self.user)
        self.slug = to_slug
        super().save(*args, **kwargs)

STATUS_CHOICES = (
    ('send' , 'send'),
    ('accepted' , 'accepted')
)

'''
    L'interfaccia Manager ci permette di effettuare la comunicazione con il database
    Ogni volta che si crea un insieme di query di interrogazione si fa riferimento al Manager di default 
    ma esso può essere esteso, tutto questo per organizzare al meglio il codice del programma

    In questo progetto verra usato un Manager personalizzato per gestire tutte le richieste di amicizia che un utente riceve
    , tutti gli utenti il quale l'utente collegato può inviare una richiesta di amicizia e tutti i profili presenti nel sistema eccetto il
    profilo dell'utente collegato.
'''

class RelationshipManager(models.Manager):
    '''
        Metodo che restituisce tutte le richieste di amicizia ricevute
    '''
    def invites_received(self, receiver):
        # Query che filtra i risultati in base a colui che riceve la richiesta di amicizia (receiver) e lo stato della richiesta (send)
        qs = Relationship.objects.filter(receiver=receiver,status='send')
        return qs

'''
    Modello Relationship = Tabella Relationship all'interno del database
'''

class Relationship(models.Model):
    '''
        Colui che manda la richiesta di amicizia
    '''
    sender = models.ForeignKey(Profile,on_delete=models.CASCADE,related_name='sender')
    '''
        Colui che riceve la richiesta di amicizia
    '''
    receiver = models.ForeignKey(Profile,on_delete=models.CASCADE,related_name='receiver')
    '''
        Stato della richiesta di amicizia
    '''
    status = models.CharField(max_length=8, choices=STATUS_CHOICES)
    updated = models.DateTimeField(auto_now_add=True)
    created = models.DateTimeField(auto_now_add=True)

    '''
        Riga di codice che permette di estendere il Manager di default
    '''
    objects = RelationshipManager()

    def __str__(self):
        return f"{self.sender}-{self.receiver}-{self.status}"
    

''' Modello per la messaggistica '''
class Message(models.Model):
    sender_message = models.ForeignKey(Profile,on_delete=models.CASCADE,related_name='sender_message')
    receiver_message = models.ForeignKey(Profile,on_delete=models.CASCADE,related_name='receiver_message')
    content = models.TextField(default="Insert your messages", max_length=500)
    flag = models.BooleanField(default=None)
    read = models.BooleanField(default=None)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.sender_message}-{self.receiver_message}-{self.created}"