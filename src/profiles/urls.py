from django import views
from django.urls import path 
from .views import (
    all_friends_view,
    delete_friend,
    my_profile_view, 
    invites_received_view, 
    profiles_free_to_send_invites,
    ProfileDetailView,
    ProfileListView,
    search_friend,
    send_invitation,
    accept_invitation,
    reject_invitation,
    show_profile,
    chat,
    change_password
)

app_name = 'profiles'
'''
    Definendo tale path è come se indicassi --> http://127.0.0.1/profiles/myprofile
'''
urlpatterns = [
    path('', ProfileListView.as_view(), name='all-profiles-view'),
    path('myprofile/', my_profile_view, name = 'my-profile-view'),
    path('myinvites/', invites_received_view, name='my-invites-view'),
    path('toinvites/', profiles_free_to_send_invites, name='to-invite-view'),
    path('sendinvite/', send_invitation, name='send-invite'),
    path('myfriends/',all_friends_view,name='my-friends-view'),
    path('removefriend/', delete_friend, name='delete-friend-view'),
    path('searchfriend/', search_friend, name='search-friend-view'),
    path('showprofile/',show_profile, name='show-profile'),
    path('chat/', chat , name='chat-view'),
    path('sendmessage/', chat, name="send-message"),
    path('changepwd/',change_password, name='change-password'),
    path('changepwdversion/',change_password, name='change-password-view'),
    path('<slug>/', ProfileDetailView.as_view(), name='profile-detail-view'),
    path('myinvites/accept/', accept_invitation, name='accept-invite'),
    path('myinvites/acceptversion/', accept_invitation, name='accept-invite-view'),
    path('myinvites/reject/', reject_invitation, name='reject-invite'),
]
