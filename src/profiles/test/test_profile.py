from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth import get_user_model
from profiles.models import Profile

User = get_user_model()

class BaseTest(TestCase):
    def setUp(self):
        User.objects.create_user(
            username='paolorossi',
            first_name='Paolo',
            last_name='Rossi',
            password='PasswordStrong00!'
        )
        User.objects.create_user(
            username='giovanniverdi',
            first_name='Giovanni',
            last_name='Verdi',
            password='PasswordStrong00!'
        )
        self.search_url = reverse('profiles:search-friend-view')
        self.pattern = 'Giovanni'
        return super().setUp()

class ProfileTest(BaseTest):
    def test_search_friend(self):
        response = self.client.post(self.search_url, {'search': self.pattern}, format='text/html')
        self.assertEqual(response.status_code, 302)

        self.user1 = User.objects.get(id=1)
        self.user2 = User.objects.get(id=2)

        profile = User.objects.filter(first_name__startswith=self.pattern).exclude(username=self.user1) \
                    | User.objects.filter(last_name__startswith=self.pattern).exclude(username=self.user1)
        print(profile)
        self.assertTrue(profile)

