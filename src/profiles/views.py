from email import message
from logging import NullHandler
from multiprocessing import current_process
from unittest import result
from urllib import request
from webbrowser import get
from django.dispatch import receiver
from django.shortcuts import render, redirect, get_object_or_404
from matplotlib.style import context
from .models import Profile, Relationship, Message
from .forms import ProfileModelForm
from django.views.generic import ListView, DetailView
from django.contrib.auth.models import User
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.

'''
    View per la creazione del profilo
'''

@login_required
def my_profile_view(request):
    profile = Profile.objects.get(user=request.user)
    form = ProfileModelForm(request.POST or None, request.FILES or None, instance=profile)
    confirm = False

    '''
        Metodo che mi permette di emanare un segnale se il form è stato aggiornato correttamente
    '''
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            confirm = True 

    context = {
        'profile' : profile,
        'form' : form,
        'confirm' : confirm,
    }

    return render(request, 'profiles/myProfile.html', context)

'''
    View per la visualizzazione delle richieste di amicizia ricevute
'''

@login_required
def invites_received_view(request):
    '''
        Profilo corrente
    '''
    profile = Profile.objects.get(user=request.user)
    '''
        Query che filtra tutte le richieste di amicizia del profilo corrente
    '''
    qs = Relationship.objects.invites_received(profile)

    results = list(map(lambda x: x.sender, qs))

    is_empty = False
    if len(results) == 0:
        is_empty = True

    context = {
        'qs' : results,
        'is_empty': is_empty,
    }

    return render(request, 'profiles/myInvites.html', context)


@login_required
def all_friends_view(request):
    ''' Utente corrente'''
    user = request.user
    is_empty = False
    ''' Prelevo le informazioni dell'utente collegato '''
    results = Profile.objects.filter(user=user)
    ''' Prelevo la lista degli amici '''
    allFriends = list(results[0].friends.all())
    ''' Per ogni amico dell'utente collegato salvo il profilo in una lista '''
    qs = []
    for i in allFriends:
        qs.append(Profile.objects.get(user=i))
    if(len(qs) == 0):
        is_empty = True
    else:
        is_empty = False

    context = {
        'qs': qs,
        'is_empty': is_empty,
    }

    return render(request, 'profiles/myFriends.html', context)

'''
    View per accettare le richieste di amicizia
'''
@login_required
def accept_invitation(request):
    path = ''
    print(request.path)
    if request.path == '/profiles/myinvites/accept/':
        path = 'profiles:my-invites-view'
    elif request.path == '/profiles/myinvites/acceptversion/':
        path = 'profiles:my-friends-view'

    if request.method == "POST":
        pk = request.POST.get('profile_pk')
        sender = Profile.objects.get(pk=pk)
        receiver = Profile.objects.get(user=request.user)
        rel = get_object_or_404(Relationship, sender=sender, receiver=receiver)
        if rel.status == 'send':
            rel.status = 'accepted'
            rel.save()
    return redirect(path)



'''
    View per rifiutare richieste di amicizia
'''
@login_required
def reject_invitation(request):
    if request.method == "POST":
        pk = request.POST.get('profile_pk')
        sender = Profile.objects.get(pk=pk)
        receiver = Profile.objects.get(user=request.user)
        rel = get_object_or_404(Relationship, sender=sender, receiver=receiver)
        rel.delete()
    return redirect('profiles:my-invites-view')

'''
    View per la cancellazione di un utente
'''
@login_required 
def delete_friend(request):
    path = ''
    if request.path == '/profiles/removefriend/':
        path = 'profiles:my-friends-view'
    elif request.path == '/profiles/removefriendversion/':
        path = 'profiles:search-friend-view'

    if request.method == "POST":
        # Prelevo la chiave primari dell'utente che voglio rimuovere
        pk = request.POST.get('profile_pk')
        # Prelevo l'utente collegato
        currentUser = Profile.objects.get(user=request.user)
        # Ricerco il profilo all'interno della tabella, generando una ricerca in base alla chiave primaria
        profile = get_object_or_404(Profile,pk=pk)
        # Prelevo la lista completa di tutti gli amici dell'utente collegato
        allFriends = list(currentUser.friends.all())
        # Prelevo la relazione che associa l'utente collegato all'utente che si vuole eliminare
        rel = Relationship.objects.filter(status='accepted', sender=profile) \
                | Relationship.objects.filter(status='accepted', receiver=profile)
        # Prelevo tutti i messaggi tra l'utente corrente e l'utente che voglio cancellare
        messages = Message.objects.filter(sender_message=currentUser, receiver_message=profile) \
                    | Message.objects.filter(sender_message=profile,receiver_message=currentUser).order_by('-created')
        # Cerco all'interno della lista degli amici dell'utente corrente, l'utente che dovrà essere eliminato
        for i in allFriends:
            # Quando lo trovo
            if i == profile.user:
                # Elimino l'utente dalla lista degli amici dell'utente corrente
                currentUser.friends.remove(i)
                # Elimino la relazione che lega i due utenti nella tabella Relationship 
                rel.delete()
                # Elimino tutti i messaggi
                messages.delete()
    return redirect(path)

''' Ricerca amico '''
@login_required
def search_friend(request):
    # Prelevo il pattern di ricerca inserito nell'input box 
    pattern = request.GET.get('search')
    isEmpty = False
    results = []
    # Se il pattern esiste
    if pattern:
        # Cerco tutti i profili che hanno come pattern iniziale, quello passato .. la ricerca avviene sia per nome che per cognome
        # escludendo l'utente collegato
        results = Profile.objects.filter(first_name__startswith=pattern).exclude(user=request.user) \
                    | Profile.objects.filter(last_name__startswith=pattern).exclude(user=request.user)
        if not results:
            # Se non ci sono risultati, viene mostrato un messaggio
            isEmpty = True
    context = {
        'results': results,
        'isEmpty': isEmpty
    }
    return render(request, 'profiles/search.html', context)
    
''' Informazioni profilo ricercato '''
@login_required
def show_profile(request):
    currentUser = Profile.objects.get(user=request.user)
    check = False
    req = False
    notFriend = False
    # Prelevo la chiave primario dell'utente al quale vogli visuallizzare il profilo
    pk = request.POST.get('profile_pk')
    # Prelevo le sue informazioni tramite un'interrogazione al database
    profile = Profile.objects.get(pk=pk)

    # Se l'utente corrente ha inviato la richiesta all'utente Profile
    if (Relationship.objects.filter(status='send',sender=currentUser, receiver=profile)):
        req = True; check = False; notFriend = False;
    # Se l'utente Profile ha inviato la richiesta all'utente corrente
    elif (Relationship.objects.filter(status='send',sender=profile, receiver=currentUser)):
        req = False; check = False; notFriend = False;
    # Se tra l'utente corrente e l'utente profile è già stato stretto un rapporto di amicizia
    elif (Relationship.objects.filter(status='accepted',sender=currentUser, receiver=profile)) \
            | (Relationship.objects.filter(status='accepted',sender=profile, receiver=currentUser)):
        req = False; check = True; notFriend = False;
    # Se l'utente corrente e l'utente profilo non hanno niente a che fare l'uno con l'altro
    else:
        req = False; check = False; notFriend = True;

    context = {
        'profile': profile,
        'check': check,
        'req': req,
        'notFriend': notFriend
    }
    return render(request,'profiles/searchedProfileInfo.html', context)

'''
    View per la visualizzazione di tutti i profili presenti nel sistema dove l'utente collegato ha la possibilità
    di inviare una richiesta di amicizia
'''

@login_required
def profiles_free_to_send_invites(request):
    '''
        Utente corrente 
    '''
    user = request.user
    '''
        Query per la visualizzazione di tutti i profili presenti nel sistema escluso quello collegato e tutti i suoi amici 
    '''
    qs = Profile.objects.get_all_profiles_free_to_invite(user)

    context = {
        'qs' : qs,
    }

    return render(request, 'profiles/toInvites.html', context)

@login_required
def all_profiles_exclude_me(request):
    '''
        Utente corrente 
    '''
    user = request.user
    '''
        Query per la visualizzazione di tutti i profili presenti nel sistema escluso quello collegato
    '''
    qs = Profile.objects.get_all_profiles_exclude_me(user)

    context = {
        'qs' : qs,
    }

    return render(request, 'profiles/allProfiles.html', context)

''' Messaggistica '''
@login_required
def chat(request):
    # Lista vuota per memorizzare tutti gli amici con cui è iniziata una conversazione
    getAllFriendWithConversation = []
    # Lista vuota per contare il numero di messaggi in entrata 
    list_occurence_message = []
    # Utente corrente
    currentUser = Profile.objects.get(user=request.user)

    # Questa porzione di codice viene eseguita solo se viene visualizzata una conversazione in particolare tra gli utenti
    # con cui è iniziata una conversazione
    if request.POST.get('profile_pk'):
        # Prelevo la chiave del profilo di cui voglio visualizzare la conversazione
        pk = request.POST.get('profile_pk')
        # Prelevo dal DB l'utente in base alla chiave
        user = Profile.objects.get(pk=pk)

        # Questa porzione di codice viene eseguita solamente in seguito alla necessità di voler inviare un messaggio
        if request.path == '/profiles/sendmessage/':
            # Prelevo il contenuto del messaggio 
            content = request.POST.get('content_message')
            # Eseguo la query di creazione del messaggio
            mes = Message.objects.create(sender_message=currentUser, receiver_message=user, content=content, flag=1, read=0)

        # Filtro tutti i messaggi tra l'utente corrente e il profile del quale si necessita la visualizzazione della conversazione
        messages = Message.objects.filter(sender_message=currentUser, receiver_message=user) \
                    | Message.objects.filter(sender_message=user,receiver_message=currentUser).order_by('-created')

        # Nel momento in cui visualizzo i messaggi vado ad aggiornare il campo "read" del messaggio
        # questo per fare in modo che possa capire quando ci sono messaggi nuovi ancora non visualizzati
        notMyMessages = Message.objects.filter(sender_message=user,receiver_message=currentUser).update(read=True)

        # Questo for è stato implementato per comodità, dovuta al passaggio dei risultati nella parte di front-end
        for i in messages:
            if i.sender_message == currentUser:
                i.flag = True
            elif i.sender_message == user:
                i.flag = False
    # Se la richiesta per la visualizzazione del form dedicato alla chat non presenta riferimento a nessun utente
    # viene eseguita questa porzione di codice, questo accade quando si preme il bottone "chat" nella navbar
    else:
        user = None
        messages = None

    # Indipendentemente dal fatto che sia presente o meno un utente nella richiesta 
    # Con questo for si vanno a prelevare tutti gli utenti che fanno parte degli amici dell'utente corrente
    # con il quale è iniziata una conversazione, popolando la lista sopra citata
    for us in currentUser.friends.all():
        if Message.objects.filter(sender_message=currentUser, receiver_message=Profile.objects.get(user=us)) \
            | Message.objects.filter(sender_message=Profile.objects.get(user=us),receiver_message=currentUser):
                getAllFriendWithConversation.append(Profile.objects.get(user=us))

    # Con questo for vengono contati i messaggi ricevuti da ciascun utente presente nella lista precedente
    for item in getAllFriendWithConversation:
        if Message.objects.filter(sender_message=item,receiver_message=currentUser, read=0):
            m = Message.objects.filter(sender_message=item,receiver_message=currentUser,read=0).count()
        else:
            m = 0
        list_occurence_message.append(m)

    context = {
        'user': user,
        'currentUser': currentUser,
        'messages': messages,
        # In questa maniera ad ogni elemento [i] della lista 'getAllFriendWithConversation viene associato il corrispondente elemento
        # [j] della lista 'list_occurence_message'
        'userConversation': zip(getAllFriendWithConversation,list_occurence_message),
    }
    return render(request, 'profiles/chat.html', context)

''' Cambio password '''
@login_required
def change_password(request):
    message = ''
    error = False
    newPwd = request.GET.get('newPWD')
    confirmPwd = request.GET.get('confirmPWD')
    if newPwd != None and confirmPwd != None:
        user = User.objects.get(username=request.user)
        if newPwd == confirmPwd:
            message = "Password update correctly"
            user.set_password(newPwd)
            user.save()
            return redirect('/accounts/login')
        else:
            message = "Password doesn't match, please try again"
            error = True
        
    context = {
        'message': message,
        'error': error 
    }
    return render(request, 'profiles/changePassword.html', context)

class ProfileDetailView(LoginRequiredMixin, DetailView):
    model = Profile
    template_name = 'profiles/detail.html'

    '''def get_object(self):
        slug = self.kwargs.get('slug')
        profile = Profile.objects.get(slug=slug)
        return profile'''

    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = User.objects.get(username__iexact=self.request.user)
        profile = Profile.objects.get(user=user)
        rel_r = Relationship.objects.filter(sender=profile)
        rel_s = Relationship.objects.filter(receiver=profile)
        list_receiver = []
        list_sender = []
        for item in rel_r:
            list_receiver.append(item.receiver.user)
        for item in rel_s:
            list_sender.append(item.sender.user)
        context["list_receiver"] = list_receiver
        context["list_sender"] = list_sender
        context['posts'] = self.get_object().get_all_posts()
        context['len_posts'] = True if len(self.get_object().get_all_posts()) > 0 else False

        return context

'''
    Tale classe è in grado di svolgere lo stesso compito della funzione precedente, viene adottato questo genere 
    di programmazione per rendere il codice più leggibile
'''

class ProfileListView(LoginRequiredMixin, ListView):
    model = Profile
    template_name = 'profiles/allProfiles.html'

    '''
        Viene descritta la query che andrà ad interrogare il database per poi mostrere i risultati disponibili 
        all'utente connesso
    '''
    def get_queryset(self):
        qs = Profile.objects.get_all_profiles_exclude_me(self.request.user)
        return qs

    '''
        Tale funzione permette di aggiungere più contesti al template al quale è collegato
    '''
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        '''
            Recupero dell'utente collegato
        '''
        user = User.objects.get(username__iexact=self.request.user)
        '''
            Recupero del profilo corrente
        '''
        profile = Profile.objects.get(user=user)
        
        '''
            Profili disponibili per poter inviare una richiesta di amicizia
        '''
        rel_r = Relationship.objects.filter(sender=profile)

        '''
            Profili che hanno inviato una richiesta di amicizia al profilo corrente collegato 
        '''
        rel_s = Relationship.objects.filter(receiver=profile)

        '''
            Liste per salvare i risultati delle query precedenti
        '''
        list_receiver = []
        list_sender = []

        for item in rel_r:
            list_receiver.append(item.receiver.user)

        for item in rel_s:
            list_sender.append(item.sender.user)

        '''
            Contesti
        '''
        context["list_receiver"] = list_receiver
        context["list_sender"] = list_sender

        context["is_empty"] = False
        if len(self.get_queryset()) == 0:
            context["is_empty"] = True
            
        return context

@login_required
def send_invitation(request):
    if request.method == 'POST':
        '''
            Viene prelevata la chiave primaria dell'utente al quele si vuole inviare la richiesta di amicizia
        '''
        pk = request.POST.get('profile_pk')
        '''
            Prelevo l'utente corrente 
        '''
        user = request.user
        '''
            Imposto come mittente, colui che invia la richiesta, l'utente corrente
        '''
        sender = Profile.objects.get(user=user)
        '''
            Imposto come destinatario, colui che riceve la richiesta, l'utente in chiave primaria
        '''
        receiver = Profile.objects.get(pk=pk)

        '''
            In questa maniera si va a creare una nuova riga nella tabella relazioni, con i giusti dati inseriti
        '''
        rel = Relationship.objects.create(sender=sender,receiver=receiver,status='send')
    return redirect('profiles:search-friend-view')

@login_required
def remove_friend(request):
    if request.method == 'POST':
        '''
            Viene prelevata la chiave primaria dell'utente al quele si vuole inviare la richiesta di amicizia
        '''
        pk = request.POST.get('profile_pk')
        '''
            Prelevo l'utente corrente 
        '''
        user = request.user
        '''
            Imposto come mittente, colui che invia la richiesta, l'utente corrente
        '''
        sender = Profile.objects.get(user=user)
        '''
            Imposto come destinatario, colui che riceve la richiesta, l'utente in chiave primaria
        '''
        receiver = Profile.objects.get(pk=pk)

        '''
            Viene cercata la giusta relazione da cancellare all'interno del database.
            Due casi possibili:
                1) Nel caso l'utente corrente abbia inviato la richiesta all'utente "A" la riga in questione viene settata come (sender=sender) e (receiver=receiver)
                2) Nel caso l'utente "A" abbia inviato la richiesta all'utente corrente la riga in questione viene settata come (sender=receiver) e (receiver=sender)

            In qualsiasi caso la relazione tra utente corrente ed utente "A" è già stata accettata da uno dei due.
        '''
        rel = Relationship.objects.get((Q(sender=sender) & Q(receiver=receiver)) | (Q(sender=receiver) & Q(receiver=sender)))
        rel.delete()

        return redirect(request.META.get('HTTP_REFERER'))
    return redirect('profiles:my-profile-view')
