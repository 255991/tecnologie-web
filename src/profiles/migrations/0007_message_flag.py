# Generated by Django 4.0.2 on 2022-08-09 12:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0006_remove_message_flag'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='flag',
            field=models.BooleanField(default=None),
        ),
    ]
