# Generated by Django 4.0.2 on 2022-08-09 12:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0005_message_flag'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='message',
            name='flag',
        ),
    ]
