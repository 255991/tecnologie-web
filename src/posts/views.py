from multiprocessing import current_process
from django.core.checks import messages
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
#from jmespath import search
from matplotlib.pyplot import title
from .models import Post, Like
from profiles.models import Profile
from django.contrib.auth.models import User
from .forms import PostModelForm, CommentModelForm
from django.views.generic import UpdateView, DeleteView
from django.contrib import messages
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.

@login_required
def post_comment_create_list(request):

    search_query = request.GET.get('search', '')
    friends = request.user.friends.all()
    ''' Prelevo la lista degli amici '''
    if search_query:
        qs = Post.objects.filter(content__icontains=search_query)
    else:
        qs = Post.objects.filter(author=Profile.objects.get(user=request.user))
        for item in friends:
            qs |= Post.objects.filter(author=item).order_by('-created')
    profile = Profile.objects.get(user=request.user)

    # initials
    p_form = PostModelForm()
    c_form = CommentModelForm()
    post_added = False

    profile = Profile.objects.get(user=request.user)

    if 'submit_p_form' in request.POST:
        p_form = PostModelForm(request.POST, request.FILES)
        if p_form.is_valid():
            instace = p_form.save(commit=False)
            instace.author = profile
            instace.save()
            p_form = PostModelForm()
            post_added = True

    if 'submit_c_form' in request.POST:
        c_form = CommentModelForm(request.POST)
        if c_form.is_valid():
            instace = c_form.save(commit=False)
            instace.user = profile
            instace.post = Post.objects.get(id=request.POST.get('post_id'))
            instace.save()
            c_form = CommentModelForm

    context = {
        'qs' : qs,
        'profile' : profile,
        'p_form' : p_form,
        'c_form' : c_form,
        'post_added' : post_added,
    }

    return render(request, 'posts/myPosts.html', context)

@login_required
def like_unlike_post(request):
    '''
        Utente loggato
    '''
    user = request.user
    if request.method == 'POST':
        '''
            Prelevo l'id del post tramite una request dal form presente in myPosts.html
        '''
        post_id = request.POST.get('post_id')
        ''' 
            Avendo prelevato l'id del post ora abbiamo bisogno del post in termmine di oggetto 
        '''
        post_obj = Post.objects.get(id=post_id)
        '''
            Nel modello i like sono di tipo many-to-many ed hanno bisogno di conoscere il profilo collegato da non confondere con l'utente loggato
        '''
        profile = Profile.objects.get(user=user)
        '''
            Controllo se un determinato profilo ha messo il like al post scelto
        '''
        if profile in post_obj.liked.all():
            '''
                Se il profilo è presente una volta che si clicca sul tasto allora esso deve essere rimosso 
                Morale: se il profilo ha già messo il like e clicca di nuovo il tasto esso e come se rimuovesse il like
            '''
            post_obj.liked.remove(profile)
        else:
            '''
                Altrimenti se il profilo non è presente nei like esso viene aggiunto
            '''
            post_obj.liked.add(profile)

        like, created = Like.objects.get_or_create(user=profile,post_id = post_id)

        if not created:
            if like.value == 'Like':
                like.value = 'Unlike'
            else:
                like.value = 'Like'
        else:
            like.value = 'Like  '
            post_obj.save()
            like.save()

        data = {
            'value': like.value,
            'likes': post_obj.liked.all().count()
        }
        return JsonResponse(data, safe=False)


    return redirect('posts:main-post-view')

    
class PostDeleteView(LoginRequiredMixin, DeleteView):
    model = Post
    template_name = 'posts/confirm_del.html'
    success_url = reverse_lazy('posts:main-post-view')

    def get_object(self, *args, **kwargs):
        pk = self.kwargs.get('pk')
        obj = Post.objects.get(pk=pk)
        if not obj.author.user == self.request.user:
            messages.Warning(self.request, 'You need to be author of the post in order to delete it')
        return obj

class PostUpdateView(LoginRequiredMixin, UpdateView):
    form_class = PostModelForm
    model = Post
    template_name = 'posts/update.html'
    success_url = reverse_lazy('posts:main-post-view')

    def form_valid(self, form):
        profile = Profile.objects.get(user=self.request.user)
        if form.instance.author == profile:
            return super().form_valid(form)
        else:
            form.add_error(None, "You need to be author of the post in order to update it")
            return super().form_invalid(form)