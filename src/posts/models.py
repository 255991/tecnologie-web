from django.db import models
from django.core.validators import FileExtensionValidator
from profiles.models import Profile

# Create your models here.

class Post(models.Model):
    content = models.TextField()
    image = models.ImageField(upload_to = 'posts', blank=True, validators = [FileExtensionValidator(['png','jpg','jpeg'])])
    liked = models.ManyToManyField(Profile, blank=True, related_name = 'likes')
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='posts')

    def __str__(self):
        return str(self.content)

    '''
        Funzione che ritorna il numero complessivo dei like relativi ad un determinato utente
    '''

    def num_likes(self):
        return self.liked.all().count()

    '''
        Per contare il numero di commenti sotto ad un post non ho un riferimento all commento nella tabella post
        quindi faccio uso della seguente clausola 'comment_set' che fa riferimento a tutti i commenti 
        di un relativo post, questo avviene grazie alla relazione nata dalla dichiarazione della chiave esterna dalla tabella
        'comment' verso la tabella 'posts'
    '''

    def num_comments_on_post(self):
        return self.comment_set.all().count()

    class Meta:
        ordering = ('-created',)
    
class Comment(models.Model):
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    body = models.TextField(max_length=300)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.pk)

LIKE_CHOICES = (
    ('like', 'like'),
    ('unlike','unlike'),
)

class Like(models.Model):
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    value = models.CharField(choices=LIKE_CHOICES, max_length=8)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.user}-{self.post}-{self.value}"
    
    