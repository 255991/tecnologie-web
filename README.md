# <center>README.md</center>

<div align="center">
  <img src="images/logo.png" width="300" title="logo">
</div><br>

## <center>Progetto di tecnologie-web</center><br>

### <p>Progetto realizzato dagli studenti Capogrosso Nicolas(@NicolasCapogrosso) e Corrado Gianluca(@255991) per il sostenimento dell'esame di "Tecnologie web" del corso di laurea Informatica(FIM-INFO) dell'università degli studi di Modena e Reggio Emilia(UNIMORE).</p><br>

### <p>Sviluppatore: Nicolas Capogrosso</p>
### <p>Sistema Operativo utilizzato: Windows 11</p>
### <p> Ambiente di sviluppo: VSCode </p><br>

### <p>Sviluppatore: Gianluca Corrado</p>
### <p>Sistema Operativo: Linux</p>
### <p>Ambiente di sviluppo: VSCode</p><br>

## Librerie utilizzate
``` py
asgiref==3.5.0
asttokens==2.0.5
backcall==0.2.0
certifi==2021.10.8
cffi==1.15.0
charset-normalizer==2.0.12
colorama==0.4.5
cryptography==36.0.1
cycler==0.11.0
debugpy==1.6.0
decorator==5.1.1
defusedxml==0.7.1
distlib==0.3.4
Django==4.0.2
django-allauth==0.48.0
django-countries==7.3.2
entrypoints==0.4
executing==0.8.3
filelock==3.6.0
fonttools==4.34.2
idna==3.3
ipykernel==6.15.0
ipython==8.4.0
jedi==0.18.1
jupyter-client==7.3.4
jupyter-core==4.10.0
kiwisolver==1.4.3
matplotlib==3.5.2
matplotlib-inline==0.1.3
nest-asyncio==1.5.5
networkx==2.8.4
numpy==1.23.0
oauthlib==3.2.0
packaging==21.3
parso==0.8.3
perl==1.0.0
pickleshare==0.7.5
Pillow==9.0.1
platformdirs==2.5.1
ply==3.11
prompt-toolkit==3.0.29
psutil==5.9.1
pure-eval==0.2.2
pycparser==2.21
Pygments==2.12.0
PyJWT==2.3.0
pyparsing==3.0.9
python-dateutil==2.8.2
python3-openid==3.2.0
pywin32==304
pyzmq==23.2.0
requests==2.27.1
requests-oauthlib==1.3.1
six==1.16.0
sqlparse==0.4.2
stack-data==0.3.0
tokens==0.0.3
tornado==6.1
traitlets==5.3.0
typing_extensions==4.3.0
tzdata==2021.5
urllib3==1.26.8
virtualenv==20.13.1
wcwidth==0.2.5
```

## Download e installazione del progetto 
### 1) Se non presente installare il pacchetto Git:
```bash
apt-get install git
```
### 2) Effettuare la clonazione del progetto:
```bash
git clone https://gitlab.com/255991/tecnologie-web.git
```
```Comment
Oppure copiare la cartella del progetto
```
### 3) Se non presente installare il pacchetto python3:
```bash
apt-get install python3
```
### 4) Se non presente installare il pacchetto python3-pip:
```bash
apt-get install python3-pip
```
### 5) Installazione di Django:
```bash
# Linux
python3 -m pip install django
```
```bash
# Windows
python -m pip install django
```
### 6) Installazione di Django allauth:
```bash
# Linux
python3 -m pip install django-allauth
```
```bash
# Windows
python -m pip install django-allauth
```
### 7) Installazione di Django matplotlib:
```bash
# Linux
python3 -m pip install matplotlib
```
```bash
# Windows
python -m pip install matplotlib
```
### 8) Applicazione delle migrazioni:
```bash
# Linux
python3 manage.py migrate
```
```bash
# Windows
python manage.py migrate
```
### 9) Avvio dell'applicazione:
```bash
# Linux
python3 manage.py runserver
```
```bash
# Windows
python manage.py runserver
```
### 10) Visualizzazione della pagina principale dell'applicazione:
Sito web: <a>127.0.0.1:8000/</a>

<div class="footer" align="Center">
  <br><i color="grey">Created By Capogrosso Nicolas & Corrado Gianluca - Unimore 2020/2021.<br>
</div>

